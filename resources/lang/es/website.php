<?php

return [

  'welcome' => array(
      'heading' => 'Bienvenido a Event Reminder, por cortesía de 400tres.com',
      'body' => 'Cree sus eventos y programe recordatorios para quien usted desee.',
      'advice' => '',
  ),
    'dashboard' => array(
        'no_event' => 'Todavía no ha creado eventos',
        'add_event' => 'Presione el boton para crear un evento',
        'add_event_btn' => 'CREAR NUEVO EVENTO',
        'event_date' => 'Fecha',
        'event_description' => 'Descripción',
        'show_reminder' => 'Mostrar Recordatorio',
        'set_reminder' => 'Poner Recoradatorio',
    ),
    'reminder' => array(

        'event_date' => 'Fecha',
        'event_title' => 'Evento',
        'event_description' => 'Descripción',
        'on' => 'El día',
        'remind_to' => 'Recordar a',
        'create_event' => 'AGREGAR RECORDATORIO',
    ),
    'modals' => array(
          'event_date' => 'Fecha',
          'event_title' => 'Evento',
          'event_description' => 'Descripción',
          'create_event' => 'CREAR EVENTO',
          'create_reminder' => 'AGREGA RECORDATORIO',
          'update_event' => 'ACTUALIZAR UN EVENTO',
          'update_reminder' => 'ACTUALIZAR UN RECORDATORIO',
          'reminder_date'=> 'Fecha del recordatorio',
          'reminder_message'=> 'Mensaje del recordatorio',
          'remind_to'=> 'Recordar a',
          'remind_to_placeholder'=> 'e-mail@dominio.com',
          'reminder_message_placeholder' => 'Mensaje',
    ),

    'layouts' => array(
          'toggle_navitagion' => 'Colapsar',
          'brand' => '?',
          'dashboard' => 'Eventos',
          'today_reminder' => 'Recordatorios de Hoy',

    ),
    'reset' => 'Su contraseña se ha reseteado!',
    'sent' => 'Hemos enviado el enlace para resetear la contraseña a su correo electrónico!',
    'token' => 'El token para resetear el password caducó o es inválido.',
    'user' => 'No se encontraron registros con esa direccion de correo electrónico.',
    'update' => 'Actualizar',
    'delete' => 'Eliminar',
    'save' => 'Guardar',
    'cancel' => 'Cancelar',
    'login' => 'Ingresar',
    'register' => 'Registrarse',
    'logout' => 'Salir',



];
