<?php

return [

    'failed' => 'Error de correo o contraseña.',
    'throttle' => 'Se detectaron varios intento fallidos de ingreso. Intente en :seconds segundos otra vez.',

];
