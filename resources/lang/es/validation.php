<?php

return [


    'accepted'             => 'El :attribute debe ser aceptado.',
    'active_url'           => 'El :attribute no es una URL valida.',
    'after'                => 'El :attribute debe ser una fecha despues de :date.',
    'alpha'                => 'El :attribute debe contener solamente letras.',
    'alpha_dash'           => 'El :attribute debe contener solamente letras, numeros, y guiones.',
    'alpha_num'            => 'El :attribute debe contener solamente letras y numeros.',
    'array'                => 'El :attribute debe ser un array.',
    'before'               => 'El :attribute debe ser una fecha anterior a :date.',
    'between'              => [
        'numeric' => 'El :attribute debe ser entre :min y :max.',
        'file'    => 'El :attribute debe ser entre :min y :max kilobytes.',
        'string'  => 'El :attribute debe ser entre :min y :max caracteres.',
        'array'   => 'El :attribute debe tener entre :min y :max items.',
    ],
    'boolean'              => 'El :attribute field debe ser true o false.',
    'confirmed'            => 'El :attribute confirmation no coincide.',
    'date'                 => 'El :attribute no es una fecha valida.',
    'date_format'          => 'El :attribute no coincide el formato :format.',
    'different'            => 'El :attribute y :other deben ser diferente.',
    'digits'               => 'El :attribute debe ser :digits dígitos.',
    'digits_between'       => 'El :attribute debe ser entre :min y :max dígitos.',
    'distinct'             => 'El :attribute field tiene un valor duplicado.',
    'email'                => 'El :attribute debe ser un correo electrónico válido.',
    'exists'               => 'El :attribute seleccionado es invalido.',
    'filled'               => 'El :attribute field es requerido.',
    'image'                => 'El :attribute debe ser una imagen.',
    'in'                   => 'El selected :attribute es invalido.',
    'in_array'             => 'El :attribute field no existe en :other.',
    'integer'              => 'El :attribute debe ser un integer.',
    'ip'                   => 'El :attribute debe ser una IP válida.',
    'json'                 => 'El :attribute debe ser una cadena JSON válida.',
    'max'                  => [
        'numeric' => 'El :attribute may not be greater than :max.',
        'file'    => 'El :attribute may not be greater than :max kilobytes.',
        'string'  => 'El :attribute may not be greater than :max caracteres.',
        'array'   => 'El :attribute may not have more than :max items.',
    ],
    'mimes'                => 'El :attribute debe ser a un fichero de tipo: :values.',
    'min'                  => [
        'numeric' => 'El :attribute debe ser de al menos :min.',
        'file'    => 'El :attribute debe ser de al menos :min kilobytes.',
        'string'  => 'El :attribute debe ser de al menos :min caracteres.',
        'array'   => 'El :attribute debe tenera al menos :min items.',
    ],
    'not_in'               => 'El :attribute seleccionado es invalido.',
    'numeric'              => 'El :attribute debe ser a number.',
    'present'              => 'El :attribute field debe ser presente.',
    'regex'                => 'El :attribute format es invalid.',
    'required'             => 'El :attribute field es requerido.',
    'required_if'          => 'El :attribute field es requerido cuando :other es :value.',
    'required_unless'      => 'El :attribute field es requerido hasta que :other este en :values.',
    'required_with'        => 'El :attribute field es requerido cuando :values es presentes.',
    'required_with_all'    => 'El :attribute field es requerido cuando :values es presentes.',
    'required_without'     => 'El :attribute field es requerido cuando :values no estan presentes.',
    'required_without_all' => 'El :attribute field es requerido cuando ninguno de :values estan presentes.',
    'same'                 => 'El :attribute y :oElr must match.',
    'size'                 => [
        'numeric' => 'El :attribute debe ser :size.',
        'file'    => 'El :attribute debe ser :size kilobytes.',
        'string'  => 'El :attribute debe ser :size caracteres.',
        'array'   => 'El :attribute debe contener :size items.',
    ],
    'string'               => 'El :attribute debe ser una cadena.',
    'timezone'             => 'El :attribute debe ser una zona horaria válida.',
    'unique'               => 'El :attribute no esta disponible.',
    'url'                  => 'El :attribute formato es invalido.',


    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    'attribute' => [],

];
