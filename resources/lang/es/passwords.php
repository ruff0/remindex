<?php

return [

    'password' => 'La contraseña debe contener al menos 6 caracteres y coincidir.',
    'reset' => 'La contraseña se ha reseteado!',
    'sent' => 'Hemos enviado el enlace para el reseteo de la contraseña a su correo electrónico!',
    'token' => 'El token para el reseteo de la contraseña no es válido.',
    'user' => "No hay usuarios registrados con ese correo electrónico.",

];
