<?php

return [
    'welcome' => array(
      'heading' => 'Welcome to Event Reminder, brought to you by 400tres.com',
      'body' => 'Create events and set reminders for yours',
      'advice' => 'As you can see the website is minimal because of the launch, we still working on developing for you, profile manger and password changer is our priority, we take commentaries, thanks for being here.',

    ),
    'dashboard' => array(
        'no_event' => 'You haven\'t create any event yet',
        'add_event' => 'Click on button bellow to create a new event!',
        'add_event_btn' => 'CREATE NEW EVENT!',
        'event_date' => 'Event date',
        'event_description' => 'Description',
        'show_reminder' => 'Show reminder',
        'set_reminder' => 'Set reminder',
    ),

    'reminder' => array(

        'event_date' => 'Date',
        'event_title' => 'Event title',
        'event_description' => 'Description',
        'on' => 'On',
        'remind_to' => 'Remind to',
        'create_event' => 'ADD REMINDER',
    ),
    'today_reminders' => array(
      'heading' => 'There are',
      'body' =>  'reminder(s) set for today and already sent.',

    ),
    'modals' => array(
          'event_date' => 'Date',
          'event_title' => 'Event title',
          'event_description' => 'Description',
          'create_event' => 'CREATE AN EVENT',
          'create_reminder' => 'CREATE AN EVENT REMINDER',
          'update_event' => 'UPDATE AN EVENT',
          'update_reminder' => 'UPDATE AN EVENT REMINDER',
          'reminder_date'=> 'Reminder date',
          'reminder_message'=> 'Reminder message',
          'remind_to'=> 'Remind to',
          'remind_to_placeholder'=> 'emailaddress@somedomain.com',
          'reminder_message_placeholder' => 'Message',
    ),

    'layouts' => array(
          'toggle_navitagion' => 'Toggle navigation',
          'brand' => '?',
          'dashboard' => 'Events',
          'today_reminders' => 'Today Reminders',

    ),

    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'update' => 'Update',
    'delete' => 'Delete',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',


];
