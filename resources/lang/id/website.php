<?php

return [
    'dashboard' => array(
        'no_event' => 'Anda belum memiliki event apapun',
        'add_event' => 'Klik tombol dibawah ini untuk membuat event baru!',
        'add_event_btn' => 'BUAT EVENT BARU!',
    ),
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

];
