<script type="text/ng-template" id="UpdateRemModal.html">
	<div class="modal-header">
		<div class="modal-title">{{ trans('website.modals.update_reminder') }}</div>
	</div>
	<div class="modal-body">
		<form>
	        <div class="form-group">
	        	<label for="reminder-date">{{ trans('website.modals.reminder_date') }}</label>
	        	<ng-datepicker ng-model="reminder.remind_date"></ng-datepicker>
	        </div>
	        <div class="form-group">
				<label for="reminder-message">{{ trans('website.modals.reminder_message') }}</label>
	        	<textarea id="reminder-message" placeholder="{{ trans('website.modals.reminder_message_placeholder') }}" class="form-control" ng-model="reminder.message"></textarea>
	        </div>
	        <div class="form-group">
	        	<label for="reminder-target">{{ trans('website.modals.remind_to') }}</label>
	        	<input id="reminder-target" placeholder="{{ trans('website.modals.remind_to_placeholder') }}" class="form-control" type="email" ng-model="reminder.remind_to">
	        </div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary" ng-click="editReminder(reminder)">{{ trans('website.save') }}</button>
		<button type="submit" class="btn btn-default" ng-click="cancel()">{{ trans('website.cancel') }}</button>
	</div>
</script>
