@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  {{ trans('website.today_reminders.heading') }}
                  {{$remindercount}}
                  {{ trans('website.today_reminders.body') }}
                </div>

                <div class="panel-body">
                  <div class="sidebar-item popular">
                    @foreach ($reminderlist as $k => $reminder)
                    <hr>

                        <ul>Message:<br> {{ $reminder [ 'message' ] }}</ul>
                        <ul>Sent to: {{ $reminder [ 'remind_to' ] }}</ul>
                        <ul>Event: {{ $reminder [ 'event']['title'] }}</ul>
                        <ul>Event Date: {{ $reminder [ 'event']['date'] }}</ul>

                    
                    </hr>
                    @endforeach

                          </div>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
