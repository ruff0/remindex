@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" ng-controller="reminderCtrl">
            <div ng-init="eventId={{ $event }}"></div>
            <div ng-repeat="(key, value) in reminders">
                <p>{{ trans('website.reminder.event_date') }} : <% value.date %></p>
                <p>{{ trans('website.reminder.event_title') }} : <% value.title %></p>
                <p>{{ trans('website.reminder.event_description') }} : <% value.description %></p>

                <a href="" ng-click="openCreateReminder(value.id)" type="button" class="btn btn-primary btn-block">{{ trans('website.reminder.create_event') }}</a>
                <br>
            <div class="panel panel-default" ng-repeat="reminder in value.event_reminders">
                <div class="panel-heading">
                    {{ trans('website.reminder.remind_to') }} : <% reminder.remind_to %>
                    <span style="float:right">
                        {{ trans('website.reminder.on') }}: <% reminder.remind_date %>
                    </span>
                </div>
                <div class="panel-body">
                    <p><% reminder.message %></p>
                </div>
                <div class="panel-footer" style="text-align:right">
                    <a href="" ng-click="updateReminderModal(reminder.id)">{{ trans('website.update') }}</a>
                     |
                     <a href="" ng-click="delReminder(reminder.id,value.id)">{{ trans('website.delete') }}</a>
                </div>
            </div>
            </div>
            @include('modals.create_reminder_modal')
            @include('modals.update_reminder_modal')
        </div>
    </div>
</div>

@endsection
