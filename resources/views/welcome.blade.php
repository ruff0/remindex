@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('website.welcome.heading') }}</div>

                <div class="panel-body">
                  {{ trans('website.welcome.body') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
